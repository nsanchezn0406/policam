/*
package com.example.normansanchez.policam.Home

import android.app.Activity
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.Window
import android.view.WindowManager
import com.example.normansanchez.policam.R


class MainActivity : Activity(), MediaPlayer.OnPreparedListener, SurfaceHolder.Callback {

    private var _mediaPlayer: MediaPlayer? = null
    private var _surfaceHolder: SurfaceHolder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Set up a full-screen black window.
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        val window = window
        window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        window.setBackgroundDrawableResource(android.R.color.black)

        setContentView(R.layout.activity_main)

        // Configure the view that renders live video.
        val surfaceView = findViewById(R.id.surfaceView) as SurfaceView
        _surfaceHolder = surfaceView.holder
        _surfaceHolder!!.addCallback(this)
        _surfaceHolder!!.setFixedSize(320, 240)
    }

    companion object {
        internal val USERNAME = "admin"
        internal val PASSWORD = "camera"
        internal val RTSP_URL = "rtsp://10.0.1.7:554/play1.sdp"
    }

    // More to come...

    */
/*
SurfaceHolder.Callback
*//*


    override fun surfaceChanged(sh: SurfaceHolder, f: Int, w: Int, h: Int) {
    }

    override fun surfaceCreated(sh: SurfaceHolder) {
        _mediaPlayer = MediaPlayer()
        _mediaPlayer!!.setDisplay(_surfaceHolder)

        val context = applicationContext
        val headers = getRtspHeaders()
        val source = Uri.parse(RTSP_URL)

        try {
            // Specify the IP camera's URL and auth headers.
            _mediaPlayer!!.setDataSource(context, source, headers)

            // Begin the process of setting up a video stream.
            _mediaPlayer!!.setOnPreparedListener(this)
            _mediaPlayer!!.prepareAsync()
        } catch (e: Exception) {
        }

    }

    override fun surfaceDestroyed(sh: SurfaceHolder) {
        _mediaPlayer!!.release()
    }

    private fun getRtspHeaders(): Map<String, String> {
        val headers = HashMap<String, String>()
        val basicAuthValue = getBasicAuthValue(USERNAME, PASSWORD)
        headers["Authorization"] = basicAuthValue
        return headers
    }

    private fun getBasicAuthValue(usr: String, pwd: String): String {
        val credentials = "$usr:$pwd"
        val flags = Base64.URL_SAFE or Base64.NO_WRAP
        val bytes = credentials.toByteArray()
        return "Basic " + Base64.encodeToString(bytes, flags)
    }

    */
/*
MediaPlayer.OnPreparedListener
*//*

    override fun onPrepared(mp: MediaPlayer) {
        _mediaPlayer!!.start()
    }
}*/
