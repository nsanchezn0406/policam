package com.example.normansanchez.policam

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.speech.RecognizerIntent
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private val RECOGNIZE_SPEECH_ACTIVITY = 1
    private var safeUser = false
    private var safePass = false
    private var user = ""
    private var password = ""

    private val db = FirebaseDatabase.getInstance()
    private val refDb = db.reference.child("usuarios")
    private val refDbInstructions = db.reference.child("instrucciones")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        initVariable()

        setListeners()
    }

    private fun initVariable() {
        user = ""
        password = ""
        etUserName.setText("")
        etPassword.setText("")
    }

    private fun setListeners() {
        tvAqui.setOnClickListener {
            val goToRegister = Intent(this, RegisterActivity::class.java)
            startActivity(goToRegister)
        }

        btnLogin.setOnClickListener {

            if (etUserName.text.toString().isEmpty() && etPassword.text.toString().isEmpty()){
                Toast.makeText(applicationContext, "Escribe tu correo electrónico y tu contraseña.android:elevation=\"4dp\"", Toast.LENGTH_SHORT).show()
            } else{
                user = etUserName.text.toString()
                password = etPassword.text.toString()

                val qUser = refDb.orderByChild("correo").equalTo(user)
                val qPass = refDb.orderByChild("password").equalTo(password)

                qUser.addListenerForSingleValueEvent(object : ValueEventListener{
                    override fun onCancelled(p0: DatabaseError) {
                        // do nothing
                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (datasnapshot in p0.getChildren()) {
                            safeUser = true
                            qPass.addListenerForSingleValueEvent(object : ValueEventListener{
                                override fun onCancelled(p0: DatabaseError) {
                                    // do nothing
                                }

                                override fun onDataChange(p0: DataSnapshot) {
                                    safePass = true
                                    Toast.makeText(applicationContext, "¡Bienvenido!", Toast.LENGTH_SHORT).show()
                                    val iniciarHomeView = Intent(applicationContext, HomeViewActivity::class.java)
                                    startActivity(iniciarHomeView)
                                }
                            })
                        }

                        if (!p0.exists()){
                            Toast.makeText(applicationContext, "Usuario y/o contraseña incorrectos, inténtalo de nuevo.", Toast.LENGTH_SHORT).show()
                        }
                    }
                })
            }

        }

        fabVoice.setOnClickListener {
            val intentActionRecognizeSpeech = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)

            intentActionRecognizeSpeech.putExtra(
                    RecognizerIntent.EXTRA_LANGUAGE_MODEL, "es-MX");
            try {
                startActivityForResult(intentActionRecognizeSpeech,
                        RECOGNIZE_SPEECH_ACTIVITY);
            } catch (a: ActivityNotFoundException) {
                Toast.makeText(applicationContext,
                        "Tú dispositivo no soporta el reconocimiento por voz", Toast.LENGTH_SHORT).show();
            }

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {

            RECOGNIZE_SPEECH_ACTIVITY -> {

                if (resultCode == Activity.RESULT_OK && null != data) {
                    val speech: ArrayList<String> = data.extras.getStringArrayList(RecognizerIntent.EXTRA_RESULTS)
                    val strSpeech2Text = speech.get(0)

                    // Esta variable guarda el resultado de lo grabado y haz lo que quieras con ella, en este caso inicia sesión
                    val grabar = strSpeech2Text

                    val qInstructions = refDbInstructions.orderByChild("instruction").equalTo(grabar)
                    qInstructions.addListenerForSingleValueEvent(object : ValueEventListener{
                        override fun onCancelled(p0: DatabaseError) {
                            // do nothing
                        }

                        override fun onDataChange(p0: DataSnapshot) {
                            for (dataSnapshot in p0.children){
                                Toast.makeText(applicationContext, "¡Bienvenido!", Toast.LENGTH_SHORT).show()
                                val iniciarHomeView = Intent(applicationContext, HomeViewActivity::class.java)
                                startActivity(iniciarHomeView)
                            }

                            if (!p0.exists()){
                                Toast.makeText(applicationContext, "No reconoce el comando de voz.", Toast.LENGTH_SHORT).show()
                            }
                        }
                    })
                }

            }
        }
    }
}

