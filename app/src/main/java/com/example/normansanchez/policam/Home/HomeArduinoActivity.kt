package com.example.normansanchez.policam.Home

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.speech.RecognizerIntent
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.widget.CompoundButton
import android.widget.Toast
import com.example.normansanchez.policam.R
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_home_arduino.*

class HomeArduinoActivity : AppCompatActivity() {

    private val RECOGNIZE_SPEECH_ACTIVITY = 1
    private val db = FirebaseDatabase.getInstance()
    private val refDbInstructions = db.reference.child("arduino-senales")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_arduino)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        setListeners()
    }

    private fun setListeners() {

        swFoco.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked){
                    val ref1 = db.reference.child("/arduino-senales/estrobo")
                    ref1.setValue(true)
                } else{
                    val ref1 = db.reference.child("/arduino-senales/estrobo")
                    ref1.setValue(false)
                }
            }
        })

        swCasa.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked){
                    val ref1 = db.reference.child("/arduino-senales/chapa")
                    ref1.setValue(true)
                } else{
                    val ref1 = db.reference.child("/arduino-senales/chapa")
                    ref1.setValue(false)
                }
            }
        })

        swSirena.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked){
                    val ref1 = db.reference.child("/arduino-senales/sirena")
                    ref1.setValue(true)
                } else{
                    val ref1 = db.reference.child("/arduino-senales/sirena")
                    ref1.setValue(false)
                }
            }
        })

        fabHome.setOnClickListener({
            val intentActionRecognizeSpeech = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)

            intentActionRecognizeSpeech.putExtra(
                    RecognizerIntent.EXTRA_LANGUAGE_MODEL, "es-MX");
            try {
                startActivityForResult(intentActionRecognizeSpeech,
                        RECOGNIZE_SPEECH_ACTIVITY);
            } catch (a: ActivityNotFoundException) {
                Toast.makeText(applicationContext,
                        "Tú dispositivo no soporta el reconocimiento por voz", Toast.LENGTH_SHORT).show();
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {

            RECOGNIZE_SPEECH_ACTIVITY -> {

                if (resultCode == Activity.RESULT_OK && null != data) {
                    val speech: ArrayList<String> = data.extras.getStringArrayList(RecognizerIntent.EXTRA_RESULTS)
                    val strSpeech2Text = speech.get(0)

                    // Esta variable guarda el resultado de lo grabado y haz lo que quieras con ella, en este caso inicia sesión
                    val grabar = strSpeech2Text

                    Toast.makeText(applicationContext, grabar, Toast.LENGTH_SHORT).show()

                    if (grabar == "abrir" || grabar == "Abrir") {
                        val refFoco = db.reference.child("/arduino-senales/chapa")
                        refFoco.setValue(true)
                        swFoco.isChecked = true
                    }

                    if (grabar == "estrobo" || grabar == "Estrobo") {
                        val refFoco = db.reference.child("/arduino-senales/estrobo")
                        refFoco.setValue(true)
                        swFoco.isChecked = true
                    }

                    if (grabar == "sirena" || grabar == "Sirena") {
                        val refFoco = db.reference.child("/arduino-senales/sirena")
                        refFoco.setValue(true)
                        swFoco.isChecked = true
                    }

                    if (grabar == "apagar" || grabar == "Apagar") {
                        val refFoco = db.reference.child("/arduino-senales/estrobo")
                        refFoco.setValue(false)
                        swFoco.isChecked = false
                    }

                    if (grabar == "desactivar" || grabar == "Desactivar") {
                        val refFoco = db.reference.child("/arduino-senales/sirena")
                        refFoco.setValue(false)
                        swFoco.isChecked = false
                    }

                    if (grabar == "cerrar" || grabar == "Cerrar") {
                        val refFoco = db.reference.child("/arduino-senales/chapa")
                        refFoco.setValue(false)
                        swFoco.isChecked = false
                    }


                }

            }
        }

    }
}
