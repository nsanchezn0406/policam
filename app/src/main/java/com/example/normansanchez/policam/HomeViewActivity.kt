package com.example.normansanchez.policam

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.speech.RecognizerIntent
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.widget.Toast
import com.example.normansanchez.policam.Home.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_home_view.*

class HomeViewActivity : AppCompatActivity() {

    private val RECOGNIZE_SPEECH_ACTIVITY = 1
    private val db = FirebaseDatabase.getInstance()
    private val refDbInstructions = db.reference.child("instrucciones")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_view)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        setListeners()
    }



    private fun setListeners() {
        ivCerrar.setOnClickListener({
            Toast.makeText(applicationContext, "¡VUELVE PRONTO!", Toast.LENGTH_SHORT).show()
            finish()
        })

        ivRepro.setOnClickListener({
            val intentOnline = Intent(this, DataStreamingActivity::class.java)
            startActivity(intentOnline)
        })

        ivMicrofono.setOnClickListener({
            val intentMicrofono = Intent(this, MicroActivity::class.java)
            startActivity(intentMicrofono)
        })

        ivCasa.setOnClickListener({
            val intentHomeArduino = Intent(this, HomeArduinoActivity::class.java)
            startActivity(intentHomeArduino)
        })

        ivEnVivo.setOnClickListener({
            val intentEnVivo = Intent(this, EnVivoActivity::class.java)
            startActivity(intentEnVivo)
        })

        fabVoice2.setOnClickListener {
            val intentActionRecognizeSpeech = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)

            intentActionRecognizeSpeech.putExtra(
                    RecognizerIntent.EXTRA_LANGUAGE_MODEL, "es-MX");
            try {
                startActivityForResult(intentActionRecognizeSpeech,
                        RECOGNIZE_SPEECH_ACTIVITY);
            } catch (a: ActivityNotFoundException) {
                Toast.makeText(applicationContext,
                        "Tú dispositivo no soporta el reconocimiento por voz", Toast.LENGTH_SHORT).show();
            }

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {

            RECOGNIZE_SPEECH_ACTIVITY -> {

                if (resultCode == Activity.RESULT_OK && null != data) {
                    val speech: ArrayList<String> = data.extras.getStringArrayList(RecognizerIntent.EXTRA_RESULTS)
                    val strSpeech2Text = speech.get(0)

                    // Esta variable guarda el resultado de lo grabado y haz lo que quieras con ella, en este caso inicia sesión
                    val grabar = strSpeech2Text

                    if (grabar == "reproducir") {
                        val qInstructions = refDbInstructions.orderByChild("instruction").equalTo(grabar)
                        qInstructions.addListenerForSingleValueEvent(object : ValueEventListener{
                            override fun onCancelled(p0: DatabaseError) {
                                // do nothing
                            }

                            override fun onDataChange(p0: DataSnapshot) {
                                for (dataSnapshot in p0.children){
                                    Toast.makeText(applicationContext, "¡Reproducir!", Toast.LENGTH_SHORT).show()
                                    val intentOnline = Intent(applicationContext, DataStreamingActivity::class.java)
                                    startActivity(intentOnline)
                                }

                                if (!p0.exists()){
                                    Toast.makeText(applicationContext, "No reconoce el comando de voz.", Toast.LENGTH_SHORT).show()
                                }
                            }
                        })
                    }

                    if (grabar == "comandos") {
                        val qInstructions = refDbInstructions.orderByChild("instruction").equalTo(grabar)
                        qInstructions.addListenerForSingleValueEvent(object : ValueEventListener{
                            override fun onCancelled(p0: DatabaseError) {
                                // do nothing
                            }

                            override fun onDataChange(p0: DataSnapshot) {
                                for (dataSnapshot in p0.children){
                                    Toast.makeText(applicationContext, "¡Comandos!", Toast.LENGTH_SHORT).show()
                                    val intentMicrofono = Intent(applicationContext, MicroActivity::class.java)
                                    startActivity(intentMicrofono)
                                }

                                if (!p0.exists()){
                                    Toast.makeText(applicationContext, "No reconoce el comando de voz.", Toast.LENGTH_SHORT).show()
                                }
                            }
                        })
                    }

                    if (grabar == "cerrar" || grabar == "cerrar sesion"){
                        val qInstructions = refDbInstructions.orderByChild("instruction").equalTo(grabar)
                        qInstructions.addListenerForSingleValueEvent(object : ValueEventListener{
                            override fun onCancelled(p0: DatabaseError) {
                                // do nothing
                            }

                            override fun onDataChange(p0: DataSnapshot) {
                                for (dataSnapshot in p0.children){
                                    Toast.makeText(applicationContext, "¡VUELVE PRONTO!", Toast.LENGTH_SHORT).show()
                                    finish()
                                }

                                if (!p0.exists()){
                                    Toast.makeText(applicationContext, "No reconoce el comando de voz.", Toast.LENGTH_SHORT).show()
                                }
                            }
                        })
                    }

                }

            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK){
            moveTaskToBack(true);
        }
        return super.onKeyDown(keyCode, event)
    }
}
