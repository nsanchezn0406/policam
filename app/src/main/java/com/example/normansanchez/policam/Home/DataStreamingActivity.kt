package com.example.normansanchez.policam.Home

import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.widget.Toast
import com.example.normansanchez.policam.R
import kotlinx.android.synthetic.main.activity_data_streaming.*

class DataStreamingActivity() : AppCompatActivity() {

    private var nameStreaming: String = ""
    private var passStreaming: String = ""
    private var adrsStreaming: String = ""
    private var myPreferences = "myPrefs"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_streaming)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        setListeners()
    }

    private fun setListeners() {
        btnCancelarStreaming.setOnClickListener{
            this.finish()
        }

        btnStreaming.setOnClickListener{
            savePreferences()

            if (nameStreaming.isEmpty() || passStreaming.isEmpty() || adrsStreaming.isEmpty()){
                Toast.makeText(applicationContext, "Debe llenar todos los datos para continuar!", Toast.LENGTH_SHORT).show()
            } else{
                val goToOnlineActivity = Intent(applicationContext, OnlineActivity::class.java)
                startActivity(goToOnlineActivity)
                this.finish()
            }
        }

    }

    private fun savePreferences() {
        val sharedPreferences = getSharedPreferences(myPreferences, Context.MODE_PRIVATE)

        nameStreaming = etNameStreaming.text.toString()
        passStreaming = etPasswordStreaming.text.toString()
        adrsStreaming = etIpAdress.text.toString()

        val editor = sharedPreferences.edit()
        editor.putString("name", nameStreaming)
        editor.putString("password", passStreaming)
        editor.putString("ipAdress", adrsStreaming)
        editor.apply()

        Toast.makeText(applicationContext, "Información lista abriendo streaming ...", Toast.LENGTH_SHORT).show()
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK){
            moveTaskToBack(true);
        }
        return super.onKeyDown(keyCode, event)
    }

}
