package com.example.normansanchez.policam.Home

import android.content.pm.ActivityInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import com.example.normansanchez.policam.R
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_micro.*

class MicroActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_micro)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        setListeners()

        showList()
    }

    private fun showList() {
        val database = FirebaseDatabase.getInstance()
        val refList = database.reference.child("instrucciones")

        refList.addValueEventListener(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                // do nothing
            }

            override fun onDataChange(p0: DataSnapshot) {

                val adapter: ArrayAdapter<String>
                val listado: ArrayList<String> = arrayListOf()

                for (datasnapshot in p0.getChildren()) {
                    val instruction = datasnapshot.child("instruction").value.toString()
                    listado.add(instruction)
                }

                adapter = ArrayAdapter(this@MicroActivity, android.R.layout.simple_list_item_1, listado)
                listView.setAdapter(adapter)

            }
        })
    }

    private fun setListeners() {
        btnGuardarIns.setOnClickListener({
            if (etInstruction.text.isEmpty()){
                Toast.makeText(applicationContext, "Escribe una instrucción", Toast.LENGTH_SHORT).show()
            } else{
                guardarInstruccionesIntoFirebase()
            }
            etInstruction.setText("")
            Toast.makeText(applicationContext, "¡Guardado!", Toast.LENGTH_SHORT).show()
        })
    }

    private fun guardarInstruccionesIntoFirebase() {
        val database = FirebaseDatabase.getInstance()
        val refInstructions = database.reference.child("instrucciones")
        refInstructions.push().child("instruction").setValue(etInstruction.text.toString())

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }


}
