package com.example.normansanchez.policam

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*


class RegisterActivity : AppCompatActivity() {

    lateinit var name    : String
    lateinit var lastName: String
    lateinit var email   : String
    lateinit var key     : String
    lateinit var password: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        initVariables()

        setLiteners()
    }

    private fun initVariables() {
        name = ""
        lastName = ""
        email = ""
        key = ""
        password = ""
    }

    private fun setLiteners() {
        btnClean.setOnClickListener {
            Toast.makeText(applicationContext, "¡No se guardo ninguna información!", Toast.LENGTH_SHORT).show()
            finish()
        }

        btnRegistrer.setOnClickListener {
            name     = etName.text.toString()
            lastName = etApellido.text.toString()
            email    = etMail.text.toString()
            key      = etCode.text.toString()
            password = etPassword.text.toString()

            registerIntoFirebase()

            Toast.makeText(applicationContext, "Te has registrado con los siguientes datos:\n" +
                    "Nombre: "+  name + "\n" +
                    "Apellido: " + lastName + "\n" +
                    "Correo Electrónico: " + email, Toast.LENGTH_LONG).show()
            val goToLogin = Intent(this, LoginActivity::class.java)
            startActivity(goToLogin)
            this.finish()

        }
    }

    private fun registerIntoFirebase() {
        val database = FirebaseDatabase.getInstance()
        val refUsuario = database.reference.child("/usuarios/$key/nombre")
        val refLastUsuario = database.reference.child("/usuarios/$key/apellido")
        val refMail = database.reference.child("/usuarios/$key/correo")
        val refPassword = database.reference.child("usuarios/$key/password")

        refUsuario.setValue(name)
        refLastUsuario.setValue(lastName)
        refMail.setValue(email)
        refPassword.setValue(password)

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK){
            moveTaskToBack(true);
        }
        return super.onKeyDown(keyCode, event)
    }

}
